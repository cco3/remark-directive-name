import visit from 'unist-util-visit'

export default (opts) => {
  return (root, f) => {
    const isLabel = n => n.data && n.data.directiveLabel
    visit(root, 'containerDirective', (node, index, parent) => {
      node.labelNode = {
        type: 'span',
        children: node.children.filter(n => isLabel(n)),
        position: node.position
      }
      node.contentsNode = {
        type: 'span',
        children: node.children.filter(n => !isLabel(n)),
        position: node.position
      }
      // For some reason, remark treats these as blank lines.
      f.messages = f.messages.filter(m =>
        m.line !== node.position.start.line + 1 ||
        m.ruleId !== 'no-consecutive-blank-lines' ||
        m.source !== 'remark-lint' ||
        m.reason !== 'Remove 1 line before node'
      )
    })

    const types = ['textDirective', 'leafDirective', 'containerDirective']
    visit(root, types, (node, index, parent) => {
      node.type = `${node.type.match(/^[a-z]*/)}-${node.name}`
      if (node.attributes.class) {
        node.attributes.class = node.attributes.class.split(/\s+/g)
      } else {
        node.attributes.class = []
      }
    })
  }
}
